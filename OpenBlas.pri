
OB_ARCH=unknown

win32 {
    win32-msvc {
        contains(QMAKE_TARGET.arch, "x86"){                     #x86
            OB_ARCH=msvc-x32
        } else {                                                #x64
            OB_ARCH=msvc-x64
        }
    } else {
        win32-g++{                                              # mingw x86 only
            OB_ARCH=mingw-x32
        }
    }
    OPEN_BLAS_FILES = $$PWD/lib/$$OB_ARCH/*.dll
    message("OpenBLAS 0.3.4 in " $$PWD/lib/$$OB_ARCH)
    LIBS += -L$$PWD/lib/$$OB_ARCH -lopenblas
    INCLUDEPATH += $$PWD/inc/$$OB_ARCH


    OPEN_BLAS_FILES_LIST = $$files($$OPEN_BLAS_FILES)

    OPEN_BLAS_LIBRARY_COPIER.name = OpenBlas library copier
    OPEN_BLAS_LIBRARY_COPIER.input = OPEN_BLAS_FILES_LIST
    OPEN_BLAS_LIBRARY_COPIER.output = $$top_builddir/${QMAKE_FILE_BASE}${QMAKE_FILE_EXT}
    OPEN_BLAS_LIBRARY_COPIER.commands = $$QMAKE_COPY \
                                        $$shell_quote($$shell_path(${QMAKE_FILE_IN})) \
                                        $$shell_quote($$shell_path(${QMAKE_FILE_OUT}))
    OPEN_BLAS_LIBRARY_COPIER.CONFIG += no_link target_predeps

    QMAKE_EXTRA_COMPILERS += OPEN_BLAS_LIBRARY_COPIER
}

unix {
    message("OpenBLAS system-wide ")
    LIBS += -lopenblas
}

DISTFILES += \
    $$PWD/README.md
